import React from 'react'
import { StaticQuery, graphql } from 'gatsby'
import Img from 'gatsby-image'

export default () => (
  <StaticQuery
    query={graphql`
      {
        file(relativePath: { eq: "gatsby-icon.png" }) {
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    `}
    render={data => (
      <div style={{ margin: '0.85rem' }}>
        <Img fluid={data.file.childImageSharp.fluid} />
        soon
      </div>
    )}
  />
)
